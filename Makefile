#!/usr/bin/env bash

fmt:
	gofmt -l -w ./

update:
	govendor remove code.sgfoot.com/...
	govendor add +e
	govendor add +l
	govendor remove +u

clean:
	rm -rf output/

build:
	go build -o output/bin/see .

run:
	output/bin/see

git:
	git pull

install: fmt clean update build

product: git fmt clean build run
