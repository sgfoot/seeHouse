{{template "header.tpl" .}}
<div class="layui-container sg-body layui-form">
    <div class="layui-row">
        <h1>{{.title}}</h1>
    </div>
    <div class="layui-row">
        <div class="layui-form-item">
            <label class="layui-form-label">手机号</label>
            <div class="layui-input-block">
                <input type="text" name="mobile" required lay-verify="required|phone" lay-verType="tips" placeholder="请输入手机号"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">暗号</label>
            <div class="layui-input-block">
                <input type="password" name="password" placeholder="第一次输入即是密码"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="ok">确认</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </div>
    <div class="layui-row">
        <blockquote class="layui-elem-quote layui-quote-nm" style="text-align: right;color:#c2c2c2">无需注册,第一次自动注册,设置一个自己的密码</blockquote>
    </div>
</div>
<script>
    //由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：
    ;!function () {
            //监听提交
         layui.use(['form'], function () {
             var form = layui.form;
             var $ = layui.jquery;

             var loginInfo = layui.data("login")
             if (loginInfo.mobile !== "") {
                 $("input[name='mobile']").val(loginInfo.mobile)
             }
             form.on('submit(ok)', function (data) {
                 $.post("/login", data.field, function (rev) {
                     if (rev.status === 0) {
                         layui.data("login", {
                             key: "mobile",
                             value: data.field.mobile
                         });
                         window.location.href = rev.data;
                     } else {
                         layer.msg(rev.msg)
                     }
                 }, 'json');
                 return false;
             });
         })
    }();
</script>
{{template "footer.tpl" .}}