{{template "header.tpl" .}}
<div class="layui-container sg-body">
    <div class="layui-row">
        <blockquote class="layui-elem-quote">{{.title}}</blockquote>
        <div class="layui-row">
            <a class="layui-btn layui-btn-sm" href="/classify/add" style="margin-bottom:5px">添加类型</a>
        </div>
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <label class="layui-form-label">类型名称</label>
                <div class="layui-input-block">
                    <input type="text" name="type_name"  required lay-verify="required" lay-verType="tips" placeholder="请输入标题"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">父级</label>
                <div class="layui-input-block">
                    <select name="parent_id" required lay-verify="required" lay-filter="parent" lay-verType="tips">
                        <option value="0" selected="selected">顶级</option>
                    {{range .Parent_list}}
                        <option value="{{.id}}">{{.type_name}}</option>
                    {{end}}
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">子项</label>
                <div class="layui-input-block" id="child">

                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-sm" lay-submit lay-filter="classifySubmit">确认</button>
                    <a href="/" class="layui-btn layui-btn-sm layui-btn-primary">返回</a>
                </div>
            </div>
        </form>
    </div>
</div>
<style>
    #child button{
        margin: 3px;
    }
</style>

<script>
    //由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：
    ;!function () {
        //Demo
        layui.use(['form', 'jquery'], function () {
            var form = layui.form
            $ = layui.jquery;
            //监听提交
            form.on('submit(classifySubmit)', function (data) {
                $.post("/classify/add", data.field, function (rev) {
                    if (rev.status === 0) {
                        console.log(rev.data)
                        if (parseInt(rev.data.parent_id) === 0) {
                            layer.msg(rev.data.name);
                            $("select[name='parent_id']").append("<option value=" + rev.data.id + ">" + rev.data.name + "</option>");
                            form.render();
                        } else {
                            $("#child").append("<button class='layui-btn layui-btn-xs layui-btn-normal'>"+rev.data.name+"</button>");
                        }
                        layer.msg(rev.msg)
                    } else {
                        layer.msg(rev.msg)
                    }
                    $("input[name='type_name']").focus().select();
                }, 'json');
                return false;
            });
            //联动子项
            form.on('select(parent)', function (data) {
                var parentId = data.value;
                $.getJSON("/classify/child", {id:parentId}, function (rev) {
                    if(rev.status === 0) {
                        $("#child").empty();
                        if (rev.data != null) {
                            $.each(rev.data, function (k, item) {
                                $("#child").append("<button class='layui-btn layui-btn-xs layui-btn-normal'>"+item.name+"</button>");
                            });
                        }
                    } else {
                        layer.msg(rev.msg);
                    }
                })
            });
        });
    }();
</script>
{{template "footer.tpl" .}}