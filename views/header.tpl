<!DOCTYPE html>
<html>
<head>
    <title>{{.title}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="/static/favicon.png" type="image/x-icon" >
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <script src="/static/layui/layui.js"></script>

    <style>
        .header-logo{
            position:absolute;
            left: 50%;
            top:50%;
            transform: translate(-50%,-50%);
            -webkit-transform: translate(-50%,-50%);
        }
        .header-img{
            display: block;
            width: 64px;
            height: 50px;
        }
        .header-right-but{
            float: right;
            margin-top: 16px;
            margin-right: 16px;
            background-color: #43ac43;
        }
        .jiantou{
            width: 10px;
            height: 10px;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            border-color: #333;
            margin-right: 3px;
            display: inline-block;
            font-size: 0;
            border-top: 2px solid rgba(0,0,0,.2);
            border-left: 2px solid rgba(0,0,0,.2);
            margin-top:24px;
            margin-left:18px;
        }
        .header-home{
            color: #333;
            font-size: 15px;
        }
        .layui-header{
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            width: 100%;
            z-index: 1000;
        }
        .sg-body{
            margin-top:70px;
        }
        h1{
            font-weight: 300;
            font-size:1.55rem;
        }
        .login-bg{
            background: linear-gradient(to left top, #051937, #004d7a, #008793, #00bf72, #a8eb12);
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            -ms-background-size: cover;
            min-height: 100vh;
            text-align: center;
        }
    </style>
</head>
<body {{if eq .page_type "login"}}class="login-bg"{{end}}>
<div class="layui-container">
    <div class="layui-header">
        {{if eq .page_type ""}}
        <a href="/" class="header-home"><i class="jiantou"></i>首页{{.title}}{{.aaa}}</a>
        {{end}}
        <a href="/" class="header-logo">
            <img class="header-img" src="/static/home.png"/>
        </a>
        {{if eq .page_type ""}}
        <a class="layui-btn header-right-but layui-btn-sm" href="/logout">安全退出</a>
        {{end}}
    </div>
</div>