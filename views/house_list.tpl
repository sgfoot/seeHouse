{{template "header.tpl" .}}
<div class="layui-container layui-form  sg-body">
    <div class="layui-row">
        <blockquote class="layui-elem-quote"><h1>{{.title}}</h1></blockquote>
    </div>
    <div class="layui-row">
        <a class="layui-btn layui-btn-sm" href="/house/add" style="margin-bottom:5px">添加房屋</a>
    </div>
    <div class="layui-row">
        <blockquote class="layui-elem-quote layui-quote-nm" style="color:#c2c2c2">列表可以左右滑动哦,提供更多操作</blockquote>
    </div>
    <table id="houseTb" lay-filter="houseFilter"></table>
</div>

<script type="text/html" id="barDemo">
<a class="layui-btn layui-btn-xs" lay-event="rate"><i class="layui-icon layui-icon-rate-half"></i>  </a>
<a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>  </a>
<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-delete"></i>  </a>
</script>
<script>
    //由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：
    ;!function () {
        layui.use(['table', 'laytpl', 'layer', 'jquery'], function(){
            var table = layui.table;
            var laytpl = layui.laytpl;
            var layer = layui.layer;
            var $ = layui.jquery;
            //第一个实例
            table.render({
                elem: '#houseTb'
                //,height: 312
                ,url: '/house/list' //数据接口
                ,method:'post'
                ,page: true //开启分页
                ,cols: [[ //表头
                    {field: 'total_score', title: '综合分', width:100, sort: true,  fixed: 'left'}
                    ,{field: 'amount', title: '租金', width:150, sort: true}
                    ,{field: 'name', title: '小区', width:150, sort: true}
                    ,{field: 'floor', title: '楼层', width:80, sort: true}
                    ,{field: 'owner', title: '业主', width:80, sort: true}
                    ,{field: 'mobile', title: '手机', width: 135,sort: true}
                    ,{field: 'score', title: '印象分', width: 80, sort: true}
                    ,{field: 'created_at', title: '添加时间', width: 150, sort: true}
                    ,{width:150, align:'center', toolbar: '#barDemo'} //这里的toolbar值是模板元素的选择器
                ]]
            });
            //监听工具条
            table.on('tool(houseFilter)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                var data = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                var tr = obj.tr; //获得当前行 tr 的DOM对象

                if(layEvent === 'rate'){ //添加星级
                    window.location.href = "/score/add?house_id=" + data.id
                } else if(layEvent === 'del'){ //删除
                    layer.confirm('真的删除行么', function(index){
                        $.getJSON("/house/del", {id:data.id}, function (rev) {
                            if (rev.status === 0) {
                                layer.msg(rev.msg)
                                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                layer.close(index);
                            } else {
                                layer.msg(rev.msg)
                            }
                        })
                        //向服务端发送删除指令
                    });
                } else if(layEvent === 'edit'){ //编辑
                    window.location.href = "/house/add?id=" + data.id
                }
            });
        });
    }();
</script>
{{template "footer.tpl" .}}