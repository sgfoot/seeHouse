{{template "header.tpl" .}}
<div class="layui-container sg-body">
    <div class="layui-row">
        <blockquote class="layui-elem-quote"><h1>{{.title}}</h1></blockquote>
    </div>
    <div class="layui-row">
        <a class="layui-btn layui-btn-sm" href="/house/list" style="margin-bottom:5px">房屋列表</a>
        <a class="layui-btn layui-btn-sm" href="/classify/add" style="margin-bottom:5px">添加类型</a>
    </div>
{{range $name, $item := .Classify}}
    <div class="layui-form">
        <div class="layui-row">
            <blockquote class="layui-elem-quote layui-quote-nm">{{$name}}</blockquote>
        {{range $key, $child := $item}}
            <div class="layui-form-item">
                <input type="hidden" name="class_id[]" value="{{$key}}">
                <label class="layui-form-label">{{$child.Name}}</label>
                <div class="layui-input-block">
                    <select name="score[]">
                        <option value="">请选择等级</option>
                        {{range $levelRow := $.scoreLevel}}
                        <option value="{{$levelRow.Key}}" {{if eq $child.Score $levelRow.Key}} selected="selected" {{end}}>{{$levelRow.Name}}</option>
                        {{end}}
                    </select>
                </div>
            </div>
        {{end}}
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn layui-btn-sm" lay-submit lay-filter="scoreSubmit">确认</button>
                <a href="/house/list" class="layui-btn layui-btn-sm layui-btn-primary">返回</a>
                <input type="hidden" value="{{$.hid}}" name="house_id">
            </div>
        </div>
    </div>
{{end}}

</div>

<script src="/static/layui/layui.js"></script>
<script>
    //由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：
    ;!function () {
        layui.use(['form', 'jquery'], function () {
            var form = layui.form,
                    $ = layui.jquery;
            //监听提交
            form.on('submit(scoreSubmit)', function (data) {
                $.post("/score/add", data.field, function (rev) {
                    if (rev.status === 0) {
                        layer.msg(rev.msg)
                    } else {
                        layer.msg(rev.msg)
                    }
                }, 'json');
                return false;
            });
        });
    }();
</script>
{{template "footer.tpl" .}}