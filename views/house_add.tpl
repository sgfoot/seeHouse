{{template "header.tpl" .}}
<div class="layui-container layui-form sg-body">
    <div class="layui-row">
        <blockquote class="layui-elem-quote"><h1>{{.title}}</h1></blockquote>
    </div>
    <div class="layui-row">
        <a class="layui-btn layui-btn-sm" href="/house/list" style="margin-bottom:5px">房屋列表</a>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">小区名称</label>
        <div class="layui-input-block">
            <input type="text" name="name" value="{{or .row.Name .null}}" required lay-verify="required" lay-verType="tips" placeholder="请输入标题"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">楼层</label>
        <div class="layui-input-block">
            <select name="floor" required lay-verify="required" lay-verType="tips">
                <option value="">选择楼层</option>
                {{range $num := .floor_num}}
                    <option value="{{$num}}"   {{if eq $num $.minFloor}} selected="selected" {{end}}>{{$num}}楼</option>
                {{end}}
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">楼高</label>
        <div class="layui-input-block">
            <select name="max_floor" required lay-verify="required" lay-verType="tips">
                <option value="">选择楼层</option>
            {{range $num := .floor_num}}
                <option value="{{$num}}" {{if eq $num $.maxFloor}} selected="selected" {{end}}>{{$num}}楼</option>
            {{end}}
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">价格</label>
        <div class="layui-input-block">
            <input type="text" name="amount" value="{{or .row.Amount .null}}" required lay-verify="required" lay-verType="tips" placeholder="请输入价格,元"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">业主</label>
        <div class="layui-input-block">
            {{range $val := .ownerList}}
            <input type="radio" name="owner" value="{{$val}}" title="{{$val}}" {{if eq $val $.owner}} checked="checked" {{end}}>
            {{end}}
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">电话</label>
        <div class="layui-input-block">
            <input type="text" name="mobile" value="{{or .row.Mobile .null}}" class="layui-input" placeholder="请输入电话">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">地址</label>
        <div class="layui-input-block">
            <input type="text" name="address" value="{{or .row.Address .null}}" class="layui-input" placeholder="请输入地址">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">印象分</label>
        <div class="layui-input-block">
            <select name="score">
                <option value="">选择印象分</option>
                {{range $val := .scoreList}}
                <option value="{{$val}}" {{if eq $val $.score}} selected="selected" {{end}}>{{$val}}</option>
                {{end}}
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block">
            <textarea name="remark" placeholder="请输入备注" class="layui-textarea">{{or .row.Remark .null}}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="houseSubmit">确认</button>
            <a href="/house/list" class="layui-btn layui-btn-primary">返回</a>
            <input type="hidden" value="{{or .id .null}}" name="id">
        </div>
    </div>
</div>

<script src="/static/layui/layui.js"></script>
<script>
    //由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：
    ;!function () {
        layui.use(['form', 'jquery'], function () {
            var form = layui.form,
            $ = layui.jquery;

            //监听提交
            form.on('submit(houseSubmit)', function (data) {
                $.post("/house/add", data.field, function (rev) {
                    if (rev.status === 0) {
                        layer.msg(rev.msg);
                        window.location.href = "/house/list"
                    } else {
                        layer.msg(rev.msg);
                    }
                }, 'json');
                return false;
            });
        });
    }();
</script>
{{template "footer.tpl" .}}