package models

import (
	"fmt"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

var o orm.Ormer

func init() {
	time.LoadLocation("Asia/Shanghai")
	orm.RegisterDriver("mysql", orm.DRMySQL)
	// set default database
	dataSource := "%s:%s@tcp(%s)/%s?charset=utf8"
	dataSource = fmt.Sprintf(dataSource, beego.AppConfig.String("mysqluser"), beego.AppConfig.String("mysqlpass"), beego.AppConfig.String("mysqlurls"), beego.AppConfig.String("mysqldb"))
	//orm.RegisterDataBase("default", "mysql", "root:123456@tcp(127.0.0.1:3307)/home?charset=utf8", 30)
	orm.RegisterDataBase("default", "mysql", dataSource, 30)
	// create table
	orm.RunSyncdb("default", false, true)
	//orm.DefaultTimeLoc = time.FixedZone("Asia/Shanghai", 8*60*60)
	orm.DefaultTimeLoc = time.UTC
	//open debug
	orm.Debug = true
	o = orm.NewOrm()
	o.Using("default")
}
