package models

import (
	"code.sgfoot.com/sg-seehome/common"
)

//插入数据
func (t *Users) Insert() error {
	t.State = 0
	t.CreatedAt = common.GetNowDateTime()
	t.UpdatedAt = common.GetNowDateTime()
	id, err := o.Insert(t)
	if err == nil {
		t.Id = int(id)
		return nil
	}
	return err
}
func (t *Users) Check(mobile string) (b bool) {
	var rs Users
	sqlText := "select * from users where mobile = ? limit 1"
	num, err := o.Raw(sqlText, mobile).QueryRows(&rs)
	if err == nil && num > 0 {
		return true
	}
	return false
}

//查询数据库
func (t *Users) SelectOne(mobile, password string) (rs Users, err error) {
	sqlText := "select * from users where mobile = ?"
	params := make([]interface{}, 0)
	params = append(params, mobile)
	if password != "" {
		sqlText += " and password = ?"
		params = append(params, password)
	}
	sqlText += " limit 1"
	err = o.Raw(sqlText, params...).QueryRow(&rs)
	return
}
