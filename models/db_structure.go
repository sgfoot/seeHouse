package models

import "github.com/astaxie/beego/orm"

// Model User
type Users struct {
	Id        int    `orm:"auto;pk"`
	Mobile    string `orm:"size(11)"`
	Password  string `orm:"size(125)"`
	IsAdmin   int    `orm:"integer(4)"`
	State     int    `orm:"integer(4)"`
	CreatedAt string `orm:"type(datetime)"`
	UpdatedAt string `orm:"type(datetime)"`
}

// Model Classify
type Classify struct {
	Id        int    `orm:"auto;pk"`
	ParentId  int    `orm:"integer(11)"`
	UserId    int    `orm:"integer(11)"`
	TypeName  string `orm:"size(25)"`
	State     int    `orm:"integer(11)"`
	CreatedAt string `orm:"type(datetime)"`
	UpdatedAt string `orm:"type(datetime)"`
}

// Model Score
type Score struct {
	Id         int    `orm:"auto;pk"`
	UserId     int    `orm:"integer(11)"`
	HouseId    int    `orm:"integer(11)"`
	ClassifyId int    `orm:"integer(11)"`
	Score      int    `orm:"integer(11)"`
	Remark     string `orm:"size(128)"`
	State      int    `orm:"integer(11)"`
	CreatedAt  string `orm:"type(datetime)"`
	UpdatedAt  string `orm:"type(datetime)"`
}

// Model House
type House struct {
	Id        int    `orm:"auto;pk"`
	UserId    int    `orm:"integer(11)"`
	Json      string `orm:"size(2048)"`
	State     int    `orm:"integer(4)"`
	CreatedAt string `orm:"type(datetime)"`
	UpdatedAt string `orm:"type(datetime)"`
}
type HouseJson struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Floor      string `json:"floor"`
	Amount     int    `json:"amount"`
	Owner      string `json:"owner"`
	Mobile     string `json:"mobile"`
	Address    string `json:"address"`
	Score      string `json:"score"` //印象分
	Remark     string `json:"remark"`
	TotalScore int    `json:"total_score"` //综合分omitempty
	CreatedAt  string `json:"created_at"`
}

func init() {
	orm.RegisterModel(new(Classify), new(Score), new(House), new(Users))
}
