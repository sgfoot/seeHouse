package models

type scoreInfo struct {
	Key  int
	Name string
}

func GetScoreLevel() []scoreInfo {
	list := []scoreInfo{
		{Key: 1, Name: "A"},
		{Key: 2, Name: "AA"},
		{Key: 3, Name: "AAA"},
		{Key: 4, Name: "AAAA"},
		{Key: 5, Name: "AAAAA"},
	}
	return list
}

//获取总积分
func (m *Score) GetTotalScore(userId int, houseId int) int {
	type Info struct {
		TotalScore int
	}
	var info Info
	sqlText := "select sum(score) as total_score from score where user_id = ? and house_id = ?"
	err := o.Raw(sqlText, userId, houseId).QueryRow(&info)
	if err != nil {
		return 0
	} else {
		return info.TotalScore
	}
}
