package routers

import (
	"code.sgfoot.com/sg-seehome/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.AdminController{}, "get:HouseList")
	beego.Router("/login", &controllers.AdminController{}, "*:Login")
	beego.Router("/logout", &controllers.AdminController{}, "get:Logout")
	beego.Router("/classify/add", &controllers.AdminController{}, "*:AddClassify")
	beego.Router("/classify/child", &controllers.AdminController{}, "get:GetClassifyChild")
	beego.Router("/score/add", &controllers.AdminController{}, "*:AddScore")
	beego.Router("/house/add", &controllers.AdminController{}, "*:HouseAdd")   //添加,处理
	beego.Router("/house/list", &controllers.AdminController{}, "*:HouseList") //房屋列表,处理
	beego.Router("/house/del", &controllers.AdminController{}, "get:HouseDel") //删除房屋列表,处理
}
