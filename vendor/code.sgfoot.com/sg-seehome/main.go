package main

import (
	_ "code.sgfoot.com/sg-seehome/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
)

func init() {
	beego.SetStaticPath("/asset", "static")
	beego.SetLogFuncCall(true)
	logs.SetLogger(logs.AdapterFile, `{"filename":"logs/project.log","level":1,"maxlines":0,"maxsize":0,"daily":true,"maxdays":10}`)
	//logs.SetLogger(logs.AdapterConn, `{"net":"tcp","addr":":7020"}`)
	//	logs.SetLogger(logs.AdapterJianLiao, `{"authorname":"seeHouse","title":"beego", "webhookurl":"https://jianliao.com/v2/services/webhook/3404975aeaf9d2c497154adc378f213f52b920b1", "redirecturl":"https://jianliao.com/v2/services/webhook/3404975aeaf9d2c497154adc378f213f52b920b1","imageurl":"https://jianliao.com/v2/services/webhook/3404975aeaf9d2c497154adc378f213f52b920b1","level":1}`)
	//	beego.BeeLogger.DelLogger("console")
	logs.Async(1000)
}

func main() {
	//beego.BConfig.WebConfig.Session.SessionProvider = "redis"
	//beego.BConfig.WebConfig.Session.SessionProviderConfig = "127.0.0.1:6379"

	beego.BConfig.WebConfig.Session.SessionOn = true
	port, _ := beego.AppConfig.Int("httpport")
	beego.BConfig.Listen.HTTPPort = port
	beego.Run()
}
