package models

import (
	"code.sgfoot.com/sg-seehome/common"
)

func GetScoreList() []string {
	return []string{
		"放弃",
		"考虑中",
		"勉强吧",
		"还行吧",
		"不错哦",
	}
}
func GetOwnerList() []string {
	return []string{
		"中介",
		"房东",
		"二房东",
	}
}

//插入数据
func (t *House) Insert() error {
	t.State = 0
	t.CreatedAt = common.GetNowDateTime()
	t.UpdatedAt = common.GetNowDateTime()
	id, err := o.Insert(t)
	t.Id = int(id)
	return err
}

func (t *House) Select(userId int) (resultList []*House, err error) {
	num, err := o.Raw("select * from house where state = 0 and user_id = ? order by created_at desc", userId).QueryRows(&resultList)
	if err == nil && num > 0 {
		return
	}
	return nil, err
}
