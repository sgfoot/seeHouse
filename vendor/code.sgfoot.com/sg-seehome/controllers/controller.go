package controllers

import (
	"fmt"
	"strings"

	"code.sgfoot.com/sg-seehome/common"
	"code.sgfoot.com/sg-seehome/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/session"
)

var GlobalSession *session.Manager

func init() {
	sessionConfig := &session.ManagerConfig{
		CookieName:      "gosessionid",
		EnableSetCookie: true,
		Gclifetime:      3600,
		Maxlifetime:     3600,
		Secure:          false,
		CookieLifeTime:  3600,
		ProviderConfig:  "./tmp",
	}
	GlobalSession, _ = session.NewManager("file", sessionConfig)
	go GlobalSession.GC()
}

type AdminController struct {
	baseController
}

//设置返回 json
func (c *AdminController) SetJson(status int, msg string, data ...interface{}) {
	_json := common.SetJson(status, msg, data...)
	c.Data["json"] = &_json
	c.ServeJSON()
}

//layui table专用返回结果
func (c *AdminController) SetJsonLayui(code int, msg string, count int, data []models.HouseJson) {
	_json := SetJsonLayer(code, msg, count, data)
	c.Data["json"] = &_json
	c.ServeJSON()
}

type baseController struct {
	beego.Controller
	o              orm.Ormer
	controllerName string
	actionName     string
}

func (p *baseController) Prepare() {
	controllerName, actionName := p.GetControllerAndAction()
	p.controllerName = strings.ToLower(controllerName[0 : len(controllerName)-10])
	p.actionName = strings.ToLower(actionName)
	p.o = orm.NewOrm()
	fmt.Printf("登陆信息. user_id:%d, is_admin:%d\n", p.GetSession("user_id"), p.GetSession("is_admin"))
	if strings.ToLower(p.controllerName) == "admin" && strings.ToLower(p.actionName) != "login" {
		t, ok := p.GetSession("user_id").(int)
		if !ok || t == 0 {
			p.History("未登录", "/login")
			p.Ctx.WriteString(p.controllerName + "===" + p.actionName)
		}
	}
	p.Data["page_type"] = ""
	////初始化前台页面相关元素
	//if strings.ToLower(p.controllerName) == "blog" {
	//	p.Data["actionName"] = strings.ToLower(actionName)
	//	var result []*models.Config
	//	p.o.QueryTable(new(models.Config).TableName()).All(&result)
	//	configs := make(map[string]string)
	//	for _, v := range result {
	//		configs[v.Name] = v.Value
	//	}
	//	p.Data["config"] = configs
	//}
}

//用来做跳转的逻辑展示
func (p *baseController) History(msg string, url string) {
	if url == "" {
		p.Ctx.WriteString("<script>alert('" + msg + "');window.history.go(-1);</script>")
		p.StopRun()
	} else {
		p.Redirect(url, 302)
	}
}

//获取用户IP地址
func (p *baseController) getClientIp() string {
	s := strings.Split(p.Ctx.Request.RemoteAddr, ":")
	return s[0]
}

func (this *baseController) setLoginSession(user models.Users) bool {
	this.SetSession("user_id", user.Id)
	this.SetSession("mobile", user.Mobile)
	this.SetSession("is_admin", user.IsAdmin)
	return true
}

type JsonLayer struct {
	Code  int                `json:"code"`
	Msg   string             `json:"msg"`
	Count int                `json:"count"`
	Data  []models.HouseJson `json:"data"`
}

//设置返回 json 结果
func SetJsonLayer(code int, msg string, count int, data []models.HouseJson) *JsonLayer {
	_json := JsonLayer{
		Code:  code,
		Msg:   msg,
		Count: count,
		Data:  data,
	}
	return &_json
}
