package common

import (
	"strconv"
	"strings"
	"time"
)

var BeijingLocation = time.FixedZone("Asia/Shanghai", 8*60*60)

//获取当前时间
func GetNowDateTime() string {
	return time.Now().In(BeijingLocation).Format("2006-01-02 15:04:05")
}

type SetJsonStruct struct {
	Status int         `json:"status"`
	Msg    string      `json:"msg"`
	Data   interface{} `json:"data"`
}

//设置返回 json 结果
func SetJson(status int, msg string, data ...interface{}) *SetJsonStruct {
	var _data interface{}
	if len(data) > 0 {
		_data = data[0]
	}
	_json := SetJsonStruct{
		Status: status,
		Msg:    msg,
		Data:   _data,
	}
	return &_json
}

//数组转string join
func ArrToStr(list []int64) string {
	listStr := make([]string, 0, len(list))
	for _, val := range list {
		listStr = append(listStr, strconv.FormatInt(val, 10))
	}
	return strings.Join(listStr, ",")
}

//生成连续的数字
func RangeNumber(max int) []int {
	arr := make([]int, 0, max)
	for i := 1; i <= max; i++ {
		arr = append(arr, i)
	}
	return arr
}
func StringToInt(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}
