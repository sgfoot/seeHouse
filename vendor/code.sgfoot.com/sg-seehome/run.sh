#!/usr/bin/env bash
flag=$1
[ -z $flag ] && flag="_"

if [ $flag = "run" ];then
    nohup output/bin/see >> logs/see.log 2>&1 &
elif [ $flag = "kill" ];then
    killall see
elif [ $flag = "install" ];then
    go build -o output/bin/see .
else
    echo "使用方法: "
    echo "安装程序: ./run.sh install "
    echo "后台永久运行: ./run.sh run "
    echo "杀死程序: ./run.sh kill "
fi